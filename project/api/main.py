import os
from celery.result import AsyncResult
from fastapi import Body, FastAPI, Request, WebSocket
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel

class Model(BaseModel):
    X: list[float]
    # Pregnancies: float
    # Glucose: float
    # BloodPressure: float
    # SkinThickness: float
    # Insulin: float
    # BMI: float
    # DiabetesPedigreeFunction: float
    # Age: float

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

from celery import Celery
worker = Celery(broker='some broker url', backend='some backend url')
worker.conf.broker_url = os.environ.get("CELERY_BROKER_URL", "redis://localhost:6379")
worker.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", "redis://localhost:6379")

@app.get("/")
async def home(request: Request):
    return templates.TemplateResponse("home.html", context={"request": request})


@app.post("/tasks", status_code=201)
async def run_task(payload = Body(...)):
    task_type = payload["type"]
    task = worker.send_task('create_task', kwargs={
    'task_type': task_type
    })
    return JSONResponse({"task_id": task.id})


@app.get("/tasks/{task_id}")
async def get_status(task_id):
    task_result = AsyncResult(task_id)
    result = {
        "task_id": task_id,
        "task_status": task_result.status,
        "task_result": task_result.result
    }
    return JSONResponse(result)


@app.post("/predict")
async def predict_model(model:Model):
    task = worker.send_task('predict_task', kwargs={
        'model': model.X
    })
    return JSONResponse({"task_id": task.id})


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        task_id = await websocket.receive_text()
        task_result = AsyncResult(task_id)
        result = {
            "task_id": task_id,
            "task_status": task_result.status,
            "task_result": task_result.result
        }
        await websocket.send_json(result)


def main():
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8004)

if __name__ == "__main__":
    main()
