// custom javascript

(function() {
  console.log('Sanity Check!');
})();

var newRow = {}; 

function handleClick(type) {
  fetch('/tasks', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ type: type }),
  })
  .then(response => response.json())
  .then(data => {
    newRow[data.task_id] = document.getElementById('tasks').insertRow(0);
    getWsStatus(data.task_id)
    //getStatus(data.task_id)
  })
}

function sendTask() {
  // получаем введеные в поля данные
  const accelerometer1RMS = document.getElementById("accelerometer1RMS").value;
  const accelerometer2RMS = document.getElementById("accelerometer2RMS").value;
  const current = document.getElementById("current").value;
  const pressure = document.getElementById("pressure").value;
  const temperature = document.getElementById("temperature").value;
  const thermocouple = document.getElementById("thermocouple").value;
  const voltage = document.getElementById("voltage").value;
  const volume_flow_rateRMS = document.getElementById("volume_flow_rateRMS").value;

  fetch('/predict', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ 
      X: [accelerometer1RMS, accelerometer2RMS, current, pressure, temperature, thermocouple, voltage, volume_flow_rateRMS]
  })
  })
  .then(response => response.json())
  .then(data => {
    newRow[data.task_id] = document.getElementById('tasks').insertRow(0);
    getStatus(data.task_id)
  })
}

function getStatus(taskID) {
  fetch(`/tasks/${taskID}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  .then(response => response.json())
  .then(res => {
    //console.log(res)
    const html = `
      <tr>
        <td>${taskID}</td>
        <td>${res.task_status}</td>
        <td>${res.task_result}</td>
      </tr>`;
      
    newRow[res.task_id].innerHTML = html;

    const taskStatus = res.task_status;
    if (taskStatus === 'SUCCESS' || taskStatus === 'FAILURE') 
    {
      document.getElementById("result").innerText = res.task_result.result == 0 ? 'Negative' : 'Positive';
      delete newRow[res.task_id];
      return false;
    }
    setTimeout(function() {
      getStatus(res.task_id);
    }, 1000);
  })
  .catch(err => console.log(err));
}


//WebSockets
var ws = new WebSocket("ws://localhost:8004/ws");

function getWsStatus(task_id) {
  ws.send(task_id)
}

function updateResult(res)
{
    const html = `
    <tr>
      <td>${res.task_id}</td>
      <td>${res.task_status}</td>
      <td>${JSON.stringify(res.task_result)}</td>
    </tr>`;
    
    newRow[res.task_id].innerHTML = html;

    const taskStatus = res.task_status;
    if (taskStatus === 'SUCCESS') 
    {
      newRow[res.task_id].style.backgroundColor = "lightgreen"; 
      document.getElementById("result").innerText = res.task_result.result == 0 ? 'Negative' : 'Positive';
      delete newRow[res.task_id];
      return false
    }
    else if (taskStatus === 'FAILURE')
    {
      newRow[res.task_id].style.backgroundColor = "lightpink"; 
      document.getElementById("result").innerText = 'Error';
      delete newRow[res.task_id];
      return false
    }
    setTimeout(function() {
      getWsStatus(res.task_id);
    }, 1000);
}

ws.onmessage = function(event) {
    res = JSON.parse(event.data)
    updateResult(res)
};

function sendWsTask() {
  // получаем введеные в поля данные
  const accelerometer1RMS = document.getElementById("accelerometer1RMS").value;
  const accelerometer2RMS = document.getElementById("accelerometer2RMS").value;
  const current = document.getElementById("current").value;
  const pressure = document.getElementById("pressure").value;
  const temperature = document.getElementById("temperature").value;
  const thermocouple = document.getElementById("thermocouple").value;
  const voltage = document.getElementById("voltage").value;
  const volume_flow_rateRMS = document.getElementById("volume_flow_rateRMS").value;

  fetch('/predict', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ 
      X: [accelerometer1RMS, accelerometer2RMS, current, pressure, temperature, thermocouple, voltage, volume_flow_rateRMS]
  })
  })
  .then(response => response.json())
  .then(data => {
    if (!data.task_id)
      return
    newRow[data.task_id] = document.getElementById('tasks').insertRow(0);
    getWsStatus(data.task_id)
  })
  .catch(err => console.log(err));
}
