import os
import time
import pickle
import numpy as np

from celery import Celery

celery = Celery(__name__)
celery.conf.broker_url = os.environ.get("CELERY_BROKER_URL", "redis://localhost:6379")
celery.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", "redis://localhost:6379")

# load the model from disk
#filename = './model/finalized_model.sav'
filename = './model/model_cb_ai_school.pkl'
loaded_model = pickle.load(open(filename, 'rb'))

@celery.task(name="create_task")
def create_task(task_type):
    time.sleep(int(task_type) * 10)
    return True

@celery.task(name="predict_task")
def predict_task(model:list[float]):
    samples_to_predict = np.array(model).reshape(1, -1)
    result = loaded_model.predict(samples_to_predict)
    return {"result": result[0]}
