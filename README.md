# Асинхронные задания с FastAPI and Celery

Пример работы с фоновыми заданиями в FastAPI, Celery и Docker.

## Как проверить без браузера?

Сборка и запуск контейнеров:

```sh
$ docker-compose up -d --build
```

Откройте в браузере страницу [http://localhost:8004](http://localhost:8004) чтобы увидеть приложение или [http://localhost:5556](http://localhost:5556) чтобы посмотреть статистику Flower.

Создание нового тестового задания:

```sh
$ curl http://localhost:8004/tasks -H "Content-Type: application/json" --data '{"type": 0}'
```

Проверка статуса задания:

```sh
$ curl http://localhost:8004/tasks/<TASK_ID>
```
